<?php

use Illuminate\Http\Request;
use App\Jobs\getProductsList;
use App\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/save_transaction/{id}', function (Request $request,$id)
{
    $name=$request->input('name');
    $value=$request->input('value');
    $date=$request->input('date');
    $transaction=DB::table('transactions')->where
    ([
        'id'=>$id
    ])->update(['name' => $name,'value' => $value,'date' => $date]);
    return json_encode($transaction);
});

Route::post('/add_transaction', function (Request $request)
{
    $user = JWTAuth::parseToken()->authenticate();
    $name=$request->input('name');
    $value=$request->input('value');
    DB::table('transactions')->insert
    ([
        'name'=>$name,
        'value'=>$value,
        'user'=>$user->id
    ]);
    
    return json_encode("OK");
})->middleware('jwt.auth');

Route::get('/transactions', function (Request $request)
{
    $user = JWTAuth::parseToken()->authenticate();
    $transactions=DB::table('transactions')->where('user',$user->id)->get();
    return $transactions;
})->middleware('jwt.auth');

Route::get('/add_transaction/{id}', function (Request $request,$id)
{
    $transaction=DB::table('transactions')
    ->where('id',$id)->get();
    //var_dump($transaction->slice(0, 1));
    return $transaction;
});

Route::get('/delete_transaction/{id}', function (Request $request,$id)
{
    $transaction=DB::table('transactions')
    ->where('id',$id)->delete();
    //var_dump($transaction->slice(0, 1));
    return json_encode("OK");
});


Route::post('get_token', 'AuthenticateController@authenticate');

Route::get('get_user', 'AuthenticateController@getAuthenticatedUser');

Route::get('refresh_token', function ()
{
    return $refreshed = JWTAuth::refresh(JWTAuth::getToken());
});

Route::get('/newuser', function()
{
    $name = "luca";
    $password = "luca";
    $user = User::create(['name' => $name,'email' => $name, 'password' => Hash::make($password)]);
});