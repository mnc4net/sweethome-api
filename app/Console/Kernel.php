<?php

namespace App\Console;

//use App\Jobs\makeJsons;
use App\Jobs\getProductsList;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->job(new getProductsList("52463",false));
        $schedule->job(new getProductsList("52463",true));
        $schedule->job(new getProductsList("52787",false));
        $schedule->job(new getProductsList("52787",true));
        $schedule->job(new getProductsList("52789",false));
        $schedule->job(new getProductsList("52789",true));
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
