<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use DateTime;
use Exception;

class getProductsList implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $affiliate;
    protected $discontinued_search;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($affiliate,$discontinued_search)
    {
        $this->affiliate=$affiliate;
        $this->discontinued_search=$discontinued_search;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $affiliate=$this->affiliate;
        $discontinued_search=$this->discontinued_search;

        if($affiliate=="52463")
        {
            $xml_path="http://www.delonghi.com/e-tale/e-tale-it-it.xml";
        }
        else if($affiliate=="52787")
        {
            $xml_path="http://www.kenwoodworld.com/e-tale/e-tale-it-it.xml";
        }
        else if($affiliate=="52789")
        {
            $xml_path="http://www.braunhousehold.com/e-tale/e-tale-it-it.xml";
        }

        $xml = simplexml_load_file($xml_path);
        $json = json_encode($xml);
        $products_feed = json_decode($json,TRUE);
        $products = collect([]);

        foreach ($products_feed['Product'] as $product_feed)
        {
            //CHECK IF MODEL NAME EXIST
            if (array_key_exists('ModelName', $product_feed))
            {
                $model_name = $product_feed['ModelName'];
            }
            else
            {
                $model_name = "No Model Name found";
            }

            //CHECK IF SKU EXIST
            if (array_key_exists('SKU', $product_feed['@attributes']))
            {
                $sku = $product_feed['@attributes']['SKU'];
            }
            else
            {
                $sku = 0;
            }

            //CHECK IF MODEL NUMBER EXIST
            if (array_key_exists('ModelNumber', $product_feed['@attributes']))
            {
                $model_number = $product_feed['@attributes']['ModelNumber'];
            }
            else
            {
                $model_number = "No Model Number found";
            }

            //CHECK IF MODEL URL
            if (array_key_exists('URL', $product_feed))
            {
                $model_url = $product_feed['URL'];
            }
            else
            {
                $model_url = "No URL found";
            }

            //CHECK IF DISCONTINUED EXIST
            if (array_key_exists('Discontinued', $product_feed['@attributes']))
            {
                $discontinued = $product_feed['@attributes']['Discontinued'];
            }
            else
            {
                $discontinued = "True";
            }

            if(!$discontinued_search)
            {
                if($discontinued=="False"||$discontinued==false||$discontinued=="false")
                {
                    $product = collect([
                        'model_name'    => $model_name, 
                        'model_number' => $model_number,
                        'model_url' => $model_url,
                        'sku'           => $sku]);
                    $products->push($product);
                }
            }
            else
            {
                if(!($discontinued=="False"||$discontinued==false||$discontinued=="false"))
                {
                    $product = collect([
                        'model_name'    => $model_name, 
                        'model_number' => $model_number,
                        'model_url' => $model_url,
                        'sku'           => $sku]);
                    $products->push($product);
                }
            }
        }

        $table=collect([]);

        $shops=collect([
            ['name'=>   'livin',        'id'=>91301],
            ['name'=>   'amazon',       'id'=>37087],
            ['name'=>   'mediaworld',   'id'=>68398],
            ['name'=>   'unieuro',      'id'=>70124],
            ['name'=>   'euronics',     'id'=>2912 ],
            ['name'=>   'expert',       'id'=>89791],
            ['name'=>   'trony',        'id'=>91331],
            ['name'=>   'comet',        'id'=>89837]
        ]);


        foreach ($products as $product)
        {
            $mpn = $product['sku'];
            $model_name = $product['model_name'];
            $model_number = $product['model_number'];
            $model_url = $product['model_url'];

            $link = "http://gethatch.com/iceleads_rest/affiliate/".$affiliate."/json?region=IT&language=IT&mpn=".$mpn;
            $json = file_get_contents($link);
            $product_feed = json_decode($json,TRUE);
            $product_feed = $product_feed['iceleads_interface'];

            $retailers=collect([]);

            foreach ($shops as $shop)
            {
                $price =        -1;
                $stock_qty =    -1;
                $link =         -1;


                if($product_feed['retailers'])
                {
                    foreach ($product_feed['retailers']['retailer'] as $retailer)
                    {
                        if($retailer['id']==$shop['id'])
                        {
                            $price =        $retailer['price'];
                            $stock_qty =    $retailer['stock_qty'];
                            $link =         $retailer['purchase_link'];
                        }

                    }
                }


                $retailer_col = collect
                ([
                    'name'      => $shop['name'],
                    'price'     => $price,
                    'stock_qty' => $stock_qty,
                    'link'      => $link
                ]);
                $retailers->push($retailer_col);
            }

            $table_row = collect([
                    'mpn'           => $mpn,
                    'model_name'    => $model_name,
                    'model_number'  => $model_number,
                    'model_url'     => $model_url,
                    'retailers'     => $retailers]);

            $table->push($table_row);
        }

        $date = new DateTime(date("d-m-Y"));
        $date = $date->format("d-m-Y");

        if($discontinued_search)
        {
            $file_path=base_path()."/public/json/".$affiliate."/".$date."_discontinued.json";
        }
        else
        {
            $file_path=base_path()."/public/json/".$affiliate."/".$date.".json";
        }

        try{unlink($file_path);}
        catch (Exception $e){}
        $fp = fopen($file_path, "w");
        fwrite($fp, json_encode($table));
        fclose($fp);

        return 1;
    }
}
